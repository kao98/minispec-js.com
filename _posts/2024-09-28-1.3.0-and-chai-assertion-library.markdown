---
layout: post
title:  "v1.3.0 and the Chai Assertion Library"
date:   2024-09-28 11:22:00 +0200
categories: minispec update
---
[MiniSpec v1.3.0](https://gitlab.com/kao98/minispec/-/releases/v1.3.0) has been released
a few days ago. Among other tings, it brings official support for the
[Chai Assertion Library](https://www.chaijs.com/).

### MiniSpec and assertion libraries

From the beginning, it was clear that MiniSpec would not implement its own assertion
library. [Node.js build-in `assert`](https://nodejs.org/api/assert.html)
would be the recommended one. It is actually used by MiniSpec for its own test suites!

{% highlight javascript %}
import { describe, context, it } from 'minispec'

// Node.js `assert`
import assert from 'assert/strict'

const theAnswer = 42

describe('Node.js `assert`', async () => {
  context('.equal', async () => {
    it('assert for equality', async () => {
      assert.equal(theAnswer, 42)
    })
  })
})
{% endhighlight %}

But it was also planned to support other assertion libraries. And actually, nothing
prevent anyone from using any existing one.

### MiniSpec v1.3.0 and Chai

Chai is an assertion library, and nothing else. While Node.js `assert` is minimalistic,
Chai brings a more comprehensive DSL, and mostly three interfaces - or styles - for
developers convenience.

MiniSpec v1.3.0 now supports Chai officially: its support is enforced in the
automated test suites.

In order to use Chai as your assertion library, add it to your dependencies and
use it as usual:

{% highlight shell %}
npm install --save-dev chai
{% endhighlight %}

{% highlight javascript %}
import { describe, context, it } from 'minispec'

// chai
import { assert, expect } from 'chai'
import 'chai/register-should';

const theAnswer = 42

describe('Chai', async () => {
  context('assert', async () => {
    it('brings `assert` syntax', async () => {
      assert.equal(theAnswer, 42)
    })
  })

  context('expect', async () => {
    it('brings `expect` syntax', async () => {
      expect(theAnswer).to.equal(42)
    })
  })

  context('should', async () => {
    it('brings `should` syntax', async () => {
      theAnswer.should.equal(42)
    })
  })
})
{% endhighlight %}
