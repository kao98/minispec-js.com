namespace :assets do
  desc "assets precompilation"
  task :precompile do
    system("jekyll build -d public", exception: true)
  end
end
